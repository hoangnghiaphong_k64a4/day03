<?php
    $gender = ['0' => "Nam", '1' => "Nữ"];
    $spe = ['MAT' => "Khoa học máy tính", 'KDL' => "Khoa học vật liệu"]
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .infor {
        display: flex;
    }

    .label_form {
        max-width: 100px;
    }

    .form {
        line-height: 30px;
        margin-top: 8px;
    }

    .label_form {
        background-color: #499ede;
        margin-right: 20px;
        color: #fff;
        border: 2px solid #0b67ad;
    }
    </style>
</head>

<body>
    <center>
        <div style="width:40% ;" class="web">
            <form>
                <div class="infor form">
                    <label class="label_form" style="flex: 1">Họ và tên</label>
                    <input style="flex: 2; border: 2px solid #0b67ad;">
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Giới tính</label>
                    <div>
                        <?php
                        for($i=0; $i< sizeof($gender); $i++ ) {
                            echo '<input style="background: #20b835" type="radio" name = "1">'.'<label style="margin-left: 6px" >'.$gender[$i].'</label>';
                        }
                    ?>
                    </div>
                </div>
                <div class="form" style="display: flex;">
                    <label style="flex:1 ;" class="label_form">Phân khoa</label>
                    <select style="border: 2px solid #0b67ad;">
                        <option selected>Trống</option>
                        <?php
                            foreach($spe as $key => $value) {
                                echo '<option>'.$value.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </form>
            <button
                style="margin-top: 20px;height: 44px; width: 120px; border-radius: 6px; border: 2px solid #0b67ad; background-color: #20b835; color: #fff; font-size: 15px;">Đăng
                ký</button>
        </div>
    </center>

</body>

</html>